Source: mygui
Priority: optional
Maintainer: Bret Curtis <psi29a@gmail.com>
Uploaders: Scott Howard <showard@debian.org>
Build-Depends: cmake, doxygen, debhelper (>= 11), g++ (>= 4:7), libogre-1.9-dev,
 pkg-kde-tools, libfreetype6-dev (>= 2.5.1), pkg-config, graphviz,
 libgl1-mesa-dev, libglew-dev, libois-dev
Homepage: http://www.ogre3d.org/tikiwiki/MyGUI
Standards-Version: 4.2.1
Section: libs
Vcs-Browser: https://salsa.debian.org/psi29a-guest/mygui
Vcs-Git: https://salsa.debian.org/psi29a-guest/mygui.git

Package: libmygui-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends},
 libmyguiengine3debian1v5 (= ${binary:Version}),
 libmygui.ogreplatform0debian1v5 (= ${binary:Version}),
 libmygui.openglplatform0debian1v5 (= ${binary:Version})
Suggests: mygui-doc
Description: Fast, simple and flexible GUI for OpenMW - development files
 MyGUI is a GUI library which aims to be fast, flexible and simple GUIs in C++.
  Features include
    Layout Editor.
    Multicolour text.
    Per pixel cut.
    Changing alpha support for widgets (also in states configuration).
    Interface localisation.
    Fast RTTI for safe casts.
    Tool tips.
    Animated cursors and pictures.
    User xml resources.
    Truetype fonts and fonts from texture.
    Widgets controllers (moving, fading and so on).
    Flexible configuration in xml config file.
    Subskins with tiling, with direct access to texture.
    Possibility to store any data in widgets items.
    Skin themes.
    Wrappers for fast UI development.
    Drag'n'drop.
 .
 This package contains the headers and static libraries needed to develop
 programs using MyGUI.

Package: libmyguiengine3debian1v5
Architecture: any
Conflicts: libmyguiengine3debian1
Replaces: libmyguiengine3debian1
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Fast, simple and flexible GUI - shared library
 MyGUI is a GUI library which aims to be fast, flexible and simple GUIs in C++.
  Features include
    Layout Editor.
    Multicolour text.
    Per pixel cut.
    Changing alpha support for widgets (also in states configuration).
    Interface localisation.
    Fast RTTI for safe casts.
    Tool tips.
    Animated cursors and pictures.
    User xml resources.
    Truetype fonts and fonts from texture.
    Widgets controllers (moving, fading and so on).
    Flexible configuration in xml config file.
    Subskins with tiling, with direct access to texture.
    Possibility to store any data in widgets items.
    Skin themes.
    Wrappers for fast UI development.
    Drag'n'drop.
 .
 This package contains the MyGUI runtime engine.

Package: libmygui.ogreplatform0debian1v5
Architecture: any
Conflicts: libmygui.ogreplatform0debian1
Replaces: libmygui.ogreplatform0debian1
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Fast, simple and flexible GUI - Ogre interface
 MyGUI is a GUI library which aims to be fast, flexible and simple GUIs in C++.
  Features include
    Layout Editor.
    Multicolour text.
    Per pixel cut.
    Changing alpha support for widgets (also in states configuration).
    Interface localisation.
    Fast RTTI for safe casts.
    Tool tips.
    Animated cursors and pictures.
    User xml resources.
    Truetype fonts and fonts from texture.
    Widgets controllers (moving, fading and so on).
    Flexible configuration in xml config file.
    Subskins with tiling, with direct access to texture.
    Possibility to store any data in widgets items.
    Skin themes.
    Wrappers for fast UI development.
    Drag'n'drop.
 .
 This package contains the MyGUI interface to the Ogre graphics engine.

Package: libmygui.openglplatform0debian1v5
Architecture: any
Conflicts: libmygui.openglplatform0debian1
Replaces: libmygui.openglplatform0debian1
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Fast, simple and flexible GUI - OpenGL interface
 MyGUI is a GUI library which aims to be fast, flexible and simple GUIs in C++.
  Features include
    Layout Editor.
    Multicolour text.
    Per pixel cut.
    Changing alpha support for widgets (also in states configuration).
    Interface localisation.
    Fast RTTI for safe casts.
    Tool tips.
    Animated cursors and pictures.
    User xml resources.
    Truetype fonts and fonts from texture.
    Widgets controllers (moving, fading and so on).
    Flexible configuration in xml config file.
    Subskins with tiling, with direct access to texture.
    Possibility to store any data in widgets items.
    Skin themes.
    Wrappers for fast UI development.
    Drag'n'drop.
 .
 This package contains the MyGUI interface for OpenGL.

Package: mygui-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}, libjs-jquery
Description: API documentations for MyGUI library
 MyGUI is a GUI library which aims to be fast, flexible and simple GUIs in C++.
  Features include
    Layout Editor.
    Multicolour text.
    Per pixel cut.
    Changing alpha support for widgets (also in states configuration).
    Interface localisation.
    Fast RTTI for safe casts.
    Tool tips.
    Animated cursors and pictures.
    User xml resources.
    Truetype fonts and fonts from texture.
    Widgets controllers (moving, fading and so on).
    Flexible configuration in xml config file.
    Subskins with tiling, with direct access to texture.
    Possibility to store any data in widgets items.
    Skin themes.
    Wrappers for fast UI development.
    Drag'n'drop.
 .
 This package contains the MyGUI API reference for the MyGUIEngine library.

